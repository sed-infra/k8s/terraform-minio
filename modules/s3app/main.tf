resource "minio_iam_user" "user" {
  name          = var.appname
  force_destroy = true
  tags = {
    app = var.appname
  }
}

resource "minio_iam_policy" "policy" {
  depends_on = [ minio_s3_bucket.buckets ]

  name = "${var.appname}-bucket-policy"
  policy = templatefile("${path.module}/policies/allowFullAccess.tmpl", {
    SID               = "allowFullAccessFor${var.appname}"
    RESOURCE_ARN_LIST = toset(sort(formatlist("arn:aws:s3:::%s/*", [ for b in minio_s3_bucket.buckets: b.id ])))
  })
}

resource "minio_iam_user_policy_attachment" "attachement" {
  user_name   = minio_iam_user.user.name
  policy_name = minio_iam_policy.policy.name
}

resource "minio_s3_bucket" "buckets" {
  bucket = "${var.appname}-${each.key}"
  acl    = each.value

  for_each = var.bucket_list
}

