variable "appname" {
  description = "Application name"
  type = string
}

variable "bucket_list" {
  description = "List of bucket name - will be prefix with '<appname>-'"
  type    = map(string)
}
