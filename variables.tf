variable "minio_server" {
  description = "Default MINIO host and port"
  type        = string
  default     = "s3.k8s.infra.cedricfarinazzo.fr"
}

variable "minio_access_key" {
  description = "MINIO user"
  type        = string
  sensitive   = true
}

variable "minio_secret_key" {
  description = "MINIO secret user"
  type        = string
  sensitive   = true
}
