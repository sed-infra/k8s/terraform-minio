## RootMl

module "cecilefarinazzofrs3app" {
  source = "./modules/s3app"

  appname = "cecilefarinazzofr"
  bucket_list = {
    "static" = "public"
  }
}
