output "rootml_secret_key" {
  value = module.rootmls3app.secret_key
}

output "gitlab_secret_key" {
  value = module.gitlabs3app.secret_key
}

output "stocks_analysis_secret_key" {
  value = module.stocksanalysiss3app.secret_key
}

output "data-storage_secret_key" {
  value = module.datastoregaes3app.secret_key
}

output "public-data_secret_key" {
  value = module.publicdatas3app.secret_key
}

output "cedricfarinazzocf_secret_key" {
  value = module.cedricfarinazzocfs3app.secret_key
}

output "cecilefarinazzofr_secret_key" {
  value = module.cecilefarinazzofrs3app.secret_key
}
