## Data storage

module "datastoregaes3app" {
  source = "./modules/s3app"

  appname = "data-storage"
  bucket_list = {
    "storage" = "private"
  }
}
