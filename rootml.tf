## RootMl

module "rootmls3app" {
  source = "./modules/s3app"

  appname = "rootml"
  bucket_list = {
    "static" = "public"
  }
}
