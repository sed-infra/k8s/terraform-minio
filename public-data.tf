## Public data

module "publicdatas3app" {
  source = "./modules/s3app"

  appname = "public-data"
  bucket_list = {
    "storage" = "public"
  }
}
