## Gitlab

module "gitlabs3app" {
  source = "./modules/s3app"

  appname = "gitlab"
  bucket_list = {
    "registry"      = "private"
    "lfs"           = "private"
    "artifacts"     = "private"
    "uploads"       = "private"
    "packages"      = "private"
    "externaldiffs" = "private"
    "terraform"     = "private"
    "pseudonymizer" = "private"
    "backup"        = "private"
    "tmp"           = "private"
  }
}
